﻿using CompSci.Fundamentals.Algorithms.Searching;
using CompSci.Fundamentals.Algorithms.Sorting.Comparison;
using CompSci.Fundamentals.Algorithms.Sorting.DivideAndConquer;
using CompSci.Fundamentals.Algorithms.Sorting.IntegerRangeBased;
using CompSci.Fundamentals.DataStructures.Hierarchical;
using CompSci.Fundamentals.DataStructures.Linear;
using CompSci.Fundamentals.ProblemSolving.Arrays;
using CompSci.Fundamentals.ProblemSolving.Graphs;
using CompSci.Fundamentals.ProblemSolving.SortingAndSearching;
using CompSci.Fundamentals.ProblemSolving.Stacks;
using CompSci.Fundamentals.ProblemSolving.Strings;
using CompSci.Fundamentals.ProblemSolving.Trees;
using CompSci.Fundamentals.Toolbox.String;
using System;
using System.Collections.Generic;

namespace CompSci.Fundamentals.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // =============== Data Structures ============== // 
            //ArrayListDriverCode.Run();
            //MyLinkedListDriverCode.Run();
            //MyStackArrayDriverCode.Run();
            //MyStackLLDriverCode.Run();
            //MyQueueLLDriverCode.Run();
            //nStackArrayDriverCode.Run();
            //HashTableDriverCode.Run();
            //MyBinaryTreeDriverCode.Run();
            //MyTrieTreeDriverCode.Run();
            //FindBuildOrderDFSDriverCode.Run();

            // ============ Sorting Algorithms ============= // 
            //BubbleSortDriverCode.Run();
            //SelectionSortDriverCode.Run();
            //InsertionSortDriverCode.Run();
            //MergeSortDriverCode.Run();
            //QuickSortDriverCode.Run();
            //CountingSortDriverCode.Run();
            //RadixSortDriverCode.Run();

            // ============ Searching Algorithms ============= // 
            //BinarySearchDriverCode.Run();

            // ============ Problem Solving ============= // 

            // Arrays
            //MaxSumOfKConsecutiveElementsDriverCode.Run();
            //LongestUniqueSubStringDriverCode.Run();
            //StringToIntegerDriverCode.Run();
            //ContainerWithMostWaterDriverCode.Run();
            //FindMedianSortedArraysDriverCode.Run();
            //SearchInRotatedSortedArrayDriverCode.Run();
            //FindKthLargestElementInArrayDriverCode.Run();
            //FindTopKFrequentElementsDriverCode.Run();
            //WordLadderLengthDriverCode.Run();
            //HeapifyABinaryTreeDriverCode.Run();

            //CutOffTreesForGolfEventDriverCode.Run();

            Console.ReadLine();
        }
    }
}

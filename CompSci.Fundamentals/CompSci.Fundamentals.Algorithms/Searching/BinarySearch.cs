﻿using System;

namespace CompSci.Fundamentals.Algorithms.Searching
{
    /// <summary>
    /// NOTE: 1.) Be very careful with your base and recursion case here!
    ///       2.) Be explicit about when you invoke your Base case. Do not let it be invoked by default or just leave as last return value.
    /// </summary>
    public class BinarySearch
    {
        public int Search(int[] inputArray, int targetValue, int startIndex, int endIndex)
        {
            // Binary Search only works on a Sorted Array. So we ensure the array is sorted
            Array.Sort(inputArray);

            int midIndex = startIndex + ((endIndex - startIndex) / 2);
            
            if (inputArray[midIndex] == targetValue)
                return midIndex;

            if (startIndex > endIndex)
                return -1;

            if (targetValue > inputArray[midIndex])
                return Search(inputArray, targetValue, midIndex + 1, endIndex);

            return Search(inputArray, targetValue, startIndex, midIndex - 1);
        }
    }

    public static class BinarySearchDriverCode
    {
        public static void Run()
        {
            Console.WriteLine("===================Binary Search=======================");

            int[] inputArraySearch = { 2, 3, 4, 7, 8, 8, 10, 40, 65, 110, 120, 121 };
            int searchTerm = 10;

            BinarySearch binarySearch = new BinarySearch();

            int result = binarySearch.Search(inputArraySearch, searchTerm, 0, inputArraySearch.Length - 1);

            if (result == -1)
                Console.WriteLine("Element NOT found in Array");
            else
                Console.WriteLine($"Element found in Array at index {result}");
        }
    }
}

﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.Comparison
{
    public class InsertionSort
    {
        public void Sort(int[] inputArray)
        {
            if (inputArray.Length == 0)
                return;

            for (int i = 1; i < inputArray.Length; i++)
            {
                // 1.) j needs to also iterate 0 as we also want the first value in the array
                // 2.) NOTE : On second for loop, we are compare 'j' to 'j-1' <=============
                for (int j = i - 1; j >= 0; j--)
                {
                    if (inputArray[j + 1] < inputArray[j])
                    {
                        // I had originally used a 'MoveIndex()' method here, which works.
                        // However, It doesn't have to be that way. We could just use a 'Swap()'
                        // This is because we are just Swapping two contigous indexes and not necessarily making shifting elements to make space
                        Swap(inputArray, j + 1, j);
                    }
                }
            }
        }

        public void Swap(int[] inputArray, int fromIndex, int toIndex)
        {
            int value = inputArray[toIndex];
            inputArray[toIndex] = inputArray[fromIndex];
            inputArray[fromIndex] = value;
        }

        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"InsertionSort => {inputArray[i]}");
        }
    }

    public static class InsertionSortDriverCode
    {
        public static void Run()
        {
            InsertionSort insertionSort = new InsertionSort();
            int[] inputArray = new int[] { 5, 8, 9, 7, 6, 2, 1, 3, 0, 4 };

            insertionSort.Sort(inputArray);
            insertionSort.PrintArray(inputArray);
        }
    }
}

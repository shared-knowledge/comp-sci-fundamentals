﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.Comparison
{
    public class BubbleSort
    {
        public void Sort(int[] inputArray)
        {
            for (int j = 0; j < inputArray.Length; j++)
            {
                int swaps = 0;
                for (int i = 0; i < inputArray.Length - 1; i++)
                {
                    if (inputArray[i + 1] < inputArray[i])
                    {
                        SwapIndex(inputArray, i + 1, i);
                        swaps++;
                    }
                }

                if (swaps == 0)
                    break;
            }
        }

        private void SwapIndex(int[] inputArray, int fromIndex, int toIndex)
        {
            int value = inputArray[toIndex];
            inputArray[toIndex] = inputArray[fromIndex];
            inputArray[fromIndex] = value;
        }

        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"BubbleSort => {inputArray[i]}");
        }
    }

    public static class BubbleSortDriverCode
    {
        public static void Run()
        {
            BubbleSort bubbleSort = new BubbleSort();
            int[] inputArray = new int[] { 5, 8, 9, 7, 6, 2, 1, 3, 0, 4 };

            bubbleSort.Sort(inputArray);

            bubbleSort.PrintArray(inputArray);
        }
    }
}

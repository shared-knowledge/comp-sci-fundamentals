﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.Comparison
{
    public class SelectionSort
    {
        public void Sort(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
            {
                // store minimum value for each iteration on inner loop
                int minValIndex = 0;
                int minval = int.MaxValue;

                for (int j = i + 1; j < inputArray.Length; j++)
                {
                    if (inputArray[j] < minval)
                    {
                        minval = inputArray[j];
                        minValIndex = j;
                    }
                }

                if (inputArray[i] > minval)
                    SwapIndex(inputArray, i, minValIndex);
            }
        }

        public void SwapIndex(int[] inputArray, int fromIndex, int toIndex)
        {
            int val = inputArray[toIndex];
            inputArray[toIndex] = inputArray[fromIndex];
            inputArray[fromIndex] = val;
        }

        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
            {
                Console.WriteLine($"SelectionSort => {inputArray[i]} ");
            }
        }
    }

    public static class SelectionSortDriverCode
    {
        public static void Run()
        {
            SelectionSort selectionSort = new SelectionSort();
            int[] inputArray = new int[] { 5, 8, 9, 7, 6, 2, 1, 3, 0, 4 };

            selectionSort.Sort(inputArray);
            selectionSort.PrintArray(inputArray);
        }
    }
}

﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.DivideAndConquer
{
    public class QuickSort
    {
        public void Sort(int[] inputArray, int start, int end)
        {
            if (start < end)
            {
                // partition inputArray, so that inputArray[sortedIndex] is at the right place
                int sortedIndex = Partition(inputArray, start, end);

                // below we Sort input array recursively, EXLCUDING the returned sortedIndex
                // as item at this index is in it's correct position
                Sort(inputArray, start, sortedIndex - 1);

                Sort(inputArray, sortedIndex + 1, end);
            }
        }

        public int Partition(int[] inputArray, int startIndex, int pivotIndex)
        {
            // NOTE : sorted index has to start outside array bounds
            //        This is incase we do NOT swap anthing, 
            //        Then when we increment, we swap Pivot at the end with startIndex
            int sortedIndex = startIndex - 1;

            for (int j = startIndex; j < pivotIndex; j++)
            {
                if (inputArray[j] <= inputArray[pivotIndex])
                {
                    SwapIndex(inputArray, j, ++sortedIndex);
                }
            }

            SwapIndex(inputArray, ++sortedIndex, pivotIndex);

            return sortedIndex;
        }

        public void SwapIndex(int[] inputArray, int fromIndex, int toIndex)
        {
            int value = inputArray[toIndex];
            inputArray[toIndex] = inputArray[fromIndex];
            inputArray[fromIndex] = value;

        }

        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"QuickSort => {inputArray[i]}");
        }
    }

    public static class QuickSortDriverCode
    {
        public static void Run()
        {
            QuickSort quickSort = new QuickSort();
            int[] inputArray = new int[] { 5, 8, 51, 9, 7, 6, 2, 1, 3, 0, 4 };

            // don't forget to do length - 1, when solving based on array index
            quickSort.Sort(inputArray, 0, inputArray.Length - 1);
            quickSort.PrintArray(inputArray);
        }
    }
}

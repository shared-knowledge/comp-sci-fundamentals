﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.DivideAndConquer
{
    /// <summary>
    /// Explanation of recursion happening here;
    ///    1.) We take in an array ---> Input Array
    ///    2.) If we need to sort more than 2 elements in the array, place an instance of this method on the call stack and reduce the array elements by half
    ///    3.) On the next instance of the method, see if we are still considering sorting more than two elements in the same array. We maintain array pointer during recursion
    ///    4.) If we are, place another instance of this method on call stack with the same array pointer
    ///    5.) When we finally get to the point where we are considering just two elements, solve for the two elements in the array and pop solve instance of method call off the stack
    ///    6.) Solve all the instances of the methods we placed on the call stack on our way here, with pointer to the same main array and two temp arrays
    /// </summary>
    public class MergeSort
    {
        public void Sort(int[] inputArray, int start, int end)
        {
            if (start < end)
            {
                int mid = start + ((end - start) / 2);

                Sort(inputArray, start, mid);
                Sort(inputArray, mid + 1, end);

                SortAndMergeArrays(inputArray, start, mid, end);
            }
        }

        // I initially copied all elements of the first array 'arr1' into mergedArray.
        // This is a HUGE mistake as all values in 'arr1' aren't necessarily smaller than those in 'arr2'
        private void SortAndMergeArrays(int[] inputArray, int startIndex, int midIndex, int endIndex)
        {
            int[] arr1 = new int[midIndex - startIndex + 1];
            int[] arr2 = new int[endIndex - midIndex];

            // NOTE : Be VERY Careful with copying array over
            for (int i = 0; i < arr1.Length; i++)
                arr1[i] = inputArray[startIndex + i];

            for (int i = 0; i < arr2.Length; i++)
                arr2[i] = inputArray[midIndex + i + 1];

            int arr1Index = 0;
            int arr2Index = 0;

            // initial index of merged array here should not be equal to 0, but rather equal to startIndex.
            int mergedIndex = startIndex;

            // NOTE :: We are over writting the inputArray below since we have copied all of it's content to arr1 and arr2
            //         This makes implementation O(n) => Auxilliary space required grows linearlly to input
            while (arr1Index < arr1.Length && arr2Index < arr2.Length)
            {
                // less than or equal to and NOT just less than here
                if (arr1[arr1Index] <= arr2[arr2Index])
                {
                    inputArray[mergedIndex] = arr1[arr1Index];
                    arr1Index++;
                }
                else
                {
                    inputArray[mergedIndex] = arr2[arr2Index];
                    arr2Index++;
                }
                mergedIndex++;
            }

            // one array might finish before the other

            // if arr1 finished before arr2, copy remained of arr2 elements in mergedArray,
            // else, copy elements of arr1 into mergedArray

            if (arr1Index < arr1.Length)
            {
                for (int i = arr1Index; i < arr1.Length; i++)
                {
                    inputArray[mergedIndex] = arr1[i];
                    mergedIndex++;
                }
            }
            
            if (arr2Index < arr2.Length)
            {
                for (int j = arr2Index; j < arr2.Length; j++)
                {
                    inputArray[mergedIndex] = arr2[j];
                    mergedIndex++;
                }
            }
        }


        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"MergeSort => {inputArray[i]} ");
        }
    }

    public static class MergeSortDriverCode
    {
        public static void Run()
        {
            MergeSort mergeSort = new MergeSort();
            int[] inputArray = new int[] { 5, 8, 51, 9, 7, 6, 2, 1, 3, 0, 4 };

            // don't forget to do length - 1 when solving based on array index
            mergeSort.Sort(inputArray, 0, inputArray.Length-1);
            mergeSort.PrintArray(inputArray);
        }
    }
}

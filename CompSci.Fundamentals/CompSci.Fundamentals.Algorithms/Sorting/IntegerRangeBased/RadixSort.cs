﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.IntegerRangeBased
{
    /// <summary>
    /// NOTE : 1.) Remember, as with counting sort, convert your Count Array to Index Array and Remember, Index Array is 1-based
    ///        2.) Scan BACKWARDS when populating output / temp array AND remember to copy back into indexArray
    ///        3.) When resolving Max Digit Count, it starts from 1. You can NOT have a digit with 0 numbers :)
    /// </summary>
    public class RadixSort
    {
        public void Sort(int[] inputArray)
        {
            // resolve inputArray max value
            int maxValue = GetMaxValue(inputArray);

            // Run count sort for each place value
            for (int pValue = 1; maxValue / pValue > 0; pValue *= 10)
                CountSortWithPlaceValue(inputArray, pValue);

            #region Or Use For Loop

            // resolve max number digit count
            int maxDigitCount = 0;
            int tempMax = maxValue;
            while (tempMax > 0)
            {
                tempMax /= 10;
                maxDigitCount++;
            }

            // run for maxDigitCount number of times
            int placeValue = 1;
            while (maxDigitCount > 0)
            {
                CountSortWithPlaceValue(inputArray, placeValue);
                placeValue *= 10;
                maxDigitCount--;
            }

            #endregion 
        }

        public void CountSortWithPlaceValue(int[] inputArray, int placeNumber)
        {
            // Create and populate count array with max value of 9.
            int maxValue = 9;
            int[] countArray = new int[maxValue + 1];

            for (int i = 0; i < inputArray.Length; i++)
                countArray[ResolvePlaceValue(inputArray[i], placeNumber)]++;

            // Convert count array to index array
            // +=. Don't make silly mistake here!!!
            for (int i = 1; i < countArray.Length; i++)
                countArray[i] += countArray[i - 1];

            // Create temp array of size = input array size and populate using correct indexes from our index array
            // SCAN BACKWARDS
            int[] temp = new int[inputArray.Length];
            for (int i = inputArray.Length - 1; i >= 0; i--)
            {
                // Get input value
                int inputValue = inputArray[i];
                // resolve input value place index
                int inputValuePlaceValue = ResolvePlaceValue(inputValue, placeNumber);
                // get input value max index from index array (i.e countArray)
                int inputValueMaxIndex = countArray[inputValuePlaceValue];

                // place input value at correct index, remembering that index array is 1-based
                temp[inputValueMaxIndex - 1] = inputValue;
                countArray[inputValuePlaceValue]--;
            }

            // Copy temp array into inputArray
            for (int i = 0; i < temp.Length; i++)
                inputArray[i] = temp[i];
        }

        public int ResolvePlaceValue(int value, int placeNumber) => (value / placeNumber) % 10;

        public int GetMaxValue(int[] inputArray)
        {
            int maxVal = int.MinValue;

            for (int i = 0; i < inputArray.Length; i++)
                if (inputArray[i] > maxVal)
                    maxVal = inputArray[i];

            return maxVal;
        }

        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"RadixSort => {inputArray[i]}");
        }
    }

    public static class RadixSortDriverCode
    {
        public static void Run()
        {
            RadixSort radixSort = new RadixSort();
            int[] inputArray = new int[] { 5, 8, 51, 29, 7, 11, 11, 6, 2, 61, 3, 0, 4 };
            //int[] inputArray = new int[] { 53, 89, 150, 36, 633, 233 };

            radixSort.Sort(inputArray); // Time Complexity => 0(3N); or O(4N) if we need to scan to find maximum value. Either way, we drop less dominant value => O(N) with alogrithm stability
            radixSort.PrintArray(inputArray);
        }
    }
}

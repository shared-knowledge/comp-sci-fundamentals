﻿using System;

namespace CompSci.Fundamentals.Algorithms.Sorting.IntegerRangeBased
{
    /// <summary>
    /// NOTE: 1.) Remember our concept of a 1-based array.
    /// </summary>
    public class CountingSort
    {
        public void Sort(int[] inputArray, int? maxValue = null)
        {
            if (maxValue == null)
                maxValue = FindMaxValue(inputArray);

            // Create and populate count array
            int[] countArray = new int[(int)maxValue + 1];

            for (int i = 0; i < inputArray.Length; i++)
                countArray[inputArray[i]]++;

            // Convert count array to 1-based index array
            for (int i = 1; i < countArray.Length; i++)
                countArray[i] += countArray[i - 1];

            // Create output array with length of input array
            int[] temp = new int[inputArray.Length];

            // Scan input array in reverse to preserve the algorithm stability
            for (int i = inputArray.Length - 1; i >= 0; i--)
            {
                int inputValue = inputArray[i];
                int inputValueMaxIndex = countArray[inputValue];

                // we minus 1 here as it is a 1-based array
                temp[inputValueMaxIndex - 1] = inputValue;
                
                countArray[inputValue]--;
            }

            // Copy temp array back into inputArray
            for (int i = 0; i < inputArray.Length; i++)
                inputArray[i] = temp[i];
        }

        // the below implementation is not stable as it does NOT maintain relative order of elements during sorting.
        // this is because we are not scanning in reverse
        // this would become evident if used as basis for Radix Sort. It just won't work for Radix Sort
        //public void Sort(int[] inputArray, int? maxValue=null)
        //{
        //    // Find Max Value in Input Array if NOT given
        //    int maxVal;

        //    if (maxValue == null)
        //        maxVal = FindMaxValue(inputArray);
        //    else
        //        maxVal = (int)maxValue;

        //    // Initialize CountArray with size of MaxValue + 1
        //    int[] countArray = new int[maxVal + 1];

        //    // Scan through InputArray and Increment Corresponding Index in CountArray
        //    for (int i = 0; i < inputArray.Length; i++)
        //    {
        //        int currentCount = countArray[inputArray[i]];
        //        countArray[inputArray[i]] = currentCount + 1;
        //    }

        //    // Re-Populate InputArray with CountArray Indexes
        //    int sortedCount = 0;
        //    for (int i = 0; i < countArray.Length; i++)
        //    {
        //        int occurenceCount = countArray[i];

        //        while (occurenceCount > 0)
        //        {
        //            inputArray[sortedCount] = i;
        //            occurenceCount--;
        //            sortedCount++;
        //        }
        //    }
        //}

        public int FindMaxValue(int[] inputArray)
        {
            int maxVal = int.MinValue;

            for (int i = 0; i < inputArray.Length; i++)
            {
                if (inputArray[i] > maxVal)
                {
                    maxVal = inputArray[i];
                }
            }

            return maxVal;
        }

        public void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"CountingSort => {inputArray[i]}");
        }
    }

    public static class CountingSortDriverCode
    {
        public static void Run()
        {
            CountingSort countingSort = new CountingSort();
            int[] inputArray = new int[] { 5, 8, 51, 29, 7, 11, 11, 6, 2, 61, 3, 0, 4 };
            //int[] inputArray = new int[] { 53, 89, 150, 36, 633, 233 };

            countingSort.Sort(inputArray); // => Time complexity of O(3N + K), N = length on input array, K = maxValue of input array? We consider only the dominant term => O(N + K)
            //countingSort.Sort(inputArray,61); // => Time complexity of O(4N + K). Same as above line, only difference here is we doing an extra scan to compute max value
            countingSort.PrintArray(inputArray);
        }
    }
}

﻿namespace CompSci.Fundamentals.DesignPatterns.Creational
{
    /// <summary>
    /// Here, the base type of "object" is used to take a lock on object which is shared across the dotnet process.
    /// This makes it Thread safe
    /// </summary>
    public sealed class Singleton
    {
        private static Singleton _instance = null;
        private static readonly object _lock = new object();

        public static Singleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock(_lock)
                    {
                        if (_instance == null)
                            _instance = new Singleton();
                    }
                }

                return _instance;
            }
        }
    }
}

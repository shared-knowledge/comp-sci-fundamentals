﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Trees
{
    public class HeapifyABinaryTree
    {
        public void Heapify(int[] inputArray)
        {
            int indexOfLastNonLeafNode = (inputArray.Length / 2) - 1;

            while (indexOfLastNonLeafNode >= 0)
                MinHeapify(inputArray, indexOfLastNonLeafNode--);
        }

        // heapify using reverse level order traversal
        public void MaxHeapify(int[] inputArray, int rootIndex)
        {
            if (rootIndex < inputArray.Length)
            {
                int leftChildIndex = (2 * rootIndex) + 1;
                int rightChildIndex = (2 * rootIndex) + 2;

                int largestIndex = rootIndex;

                if (leftChildIndex < inputArray.Length && inputArray[leftChildIndex] > inputArray[largestIndex])
                    largestIndex = leftChildIndex;

                if (rightChildIndex < inputArray.Length && inputArray[rightChildIndex] > inputArray[largestIndex])
                    largestIndex = rightChildIndex;

                if (largestIndex != rootIndex)
                {
                    Swap(inputArray, rootIndex, largestIndex);
                    MaxHeapify(inputArray, largestIndex);
                }
            }

            return;
        }

        // heapify using reverse level order traversal
        public void MinHeapify(int[] inputArray, int rootIndex)
        {
            if (rootIndex < inputArray.Length)
            {
                int leftChildIndex = (2 * rootIndex) + 1;
                int rightChildIndex = (2 * rootIndex) + 2;

                int smallestIndex = rootIndex;

                if (leftChildIndex < inputArray.Length && inputArray[leftChildIndex] < inputArray[smallestIndex])
                    smallestIndex = leftChildIndex;

                if (rightChildIndex < inputArray.Length && inputArray[rightChildIndex] < inputArray[smallestIndex])
                    smallestIndex = rightChildIndex;

                if (smallestIndex != rootIndex)
                {
                    Swap(inputArray, rootIndex, smallestIndex);
                    MinHeapify(inputArray, smallestIndex);
                }
            }

            return;
        }

        public void Swap(int[] inputArray, int fromIndex, int toIndex)
        {
            int val = inputArray[toIndex];
            inputArray[toIndex] = inputArray[fromIndex];
            inputArray[fromIndex] = val;
        }
    }


    public class MyTreeNode
    {
        public int Data { get; private set; }
        public MyTreeNode LeftNode { get; set; }
        public MyTreeNode RightNode { get; set; }

        public MyTreeNode(int data)
        {
            Data = data;
            LeftNode = null;
            RightNode = null;
        }
    }

    public static class HeapifyABinaryTreeDriverCode
    {
        public static void Run()
        {
            int[] inputArray = new int[] { 1, 3, 5, 4, 6, 13, 10, 9, 8, 15, 17 };

            HeapifyABinaryTree myBinaryTree = new HeapifyABinaryTree();
            myBinaryTree.Heapify(inputArray);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.SortingAndSearching
{
    public static class FindTopKFrequentElements
    {
        public static int[] TopKFrequent(int[] nums, int k)
        {
            if (nums.Length == 0)
                return new int[] { };

            if (nums.Length == 1)
                return nums;

            Dictionary<int, int> hashMap = new Dictionary<int, int>();

            // create hashmap of element to frequencies
            for (int i = 0; i < nums.Length; i++)
            {
                if (hashMap.ContainsKey(nums[i]))
                    hashMap[nums[i]]++;
                else
                    hashMap.Add(nums[i], hashMap.GetValueOrDefault(nums[i]) + 1);
            }

            // get unique hashmap of elements maintaining order
            int[] uniqueElements = hashMap.Select(x => x.Key).ToArray();

            // kth Largest == (N - k)th smallest
            int targetIndex = nums.Length - k;

            // run quick select
            int sortedIndex = QuickSelect(uniqueElements, 0, uniqueElements.Length - 1, targetIndex);

            return uniqueElements.Take(k).ToArray();
        }

        public static int QuickSelect(int[] inputArray, int start, int end, int targetIndex)
        {
            if (start >= end)
                return start;

            int sortedIndex = Partition(inputArray, start, end);

            if (targetIndex == sortedIndex)
                return targetIndex;
            else if (targetIndex < sortedIndex)
                return QuickSelect(inputArray, start, sortedIndex - 1, targetIndex);
            else
                return QuickSelect(inputArray, sortedIndex + 1, end, targetIndex);
        }

        public static int Partition(int[] inputArray, int start, int end)
        {
            int sortedIndex = start - 1;

            for (int i = start; i < end; i++)
                if (inputArray[i] <= inputArray[end])
                    Swap(inputArray, ++sortedIndex, end - 1);

            Swap(inputArray, ++sortedIndex, end);

            return sortedIndex;
        }

        public static void Swap(int[] inputArray, int fromIndex, int toIndex)
        {
            int val = inputArray[toIndex];
            inputArray[toIndex] = inputArray[fromIndex];
            inputArray[fromIndex] = val;
        }
    }

    public static class FindTopKFrequentElementsDriverCode
    {
        public static void Run()
        {
            int[] inputArray = new int[] { 4, 1, -1, 2, -1, 2, 3 };
            int top = 2;
            int[] maxSum = FindTopKFrequentElements.TopKFrequent(inputArray, top);

            Console.WriteLine($"Top {top}'th Frequent Element => {maxSum}.");
            PrintArray(maxSum);
            Console.WriteLine();
        }

        public static void PrintArray(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"TopKFrequent => {inputArray[i]}");
        }
    }
}

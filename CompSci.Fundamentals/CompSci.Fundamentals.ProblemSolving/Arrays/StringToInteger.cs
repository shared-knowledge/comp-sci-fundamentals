﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Arrays
{
    public static class StringToInteger
    {
        public static int MyAtoi(string s)
        {

            char[] inputArray = s.ToCharArray();

            if (inputArray.Length <= 0)
                return 0;

            if (!IsValidChar(inputArray[0]))
                return 0;

            // start index without white space
            int startIndex = 0;
            for (int i = 0; i < inputArray.Length; i++)
                if (IsValidChar(inputArray[i]))
                    if (Char.IsWhiteSpace(inputArray[i]))
                        startIndex++;
                    else
                        break;

            // declare string builder
            StringBuilder strBuilder = new StringBuilder();

            // start index without arithmetic sign
            bool hasSign = false;
            for (int i = startIndex; i < inputArray.Length; i++)
                if (IsValidChar(inputArray[i]))
                    if (inputArray[startIndex] == '-' || inputArray[startIndex] == '+')
                    {
                        if (!hasSign)
                        {
                            hasSign = true;
                            strBuilder.Append(inputArray[startIndex]);
                            startIndex++;
                        }
                        else
                            return 0;
                    }

            if (startIndex < inputArray.Length - 1)
                

            // rest of chars
            for (int i = startIndex; i < inputArray.Length; i++)
                if (IsValidChar(inputArray[i]))
                    strBuilder.Append(inputArray[i]);
                else
                    break;

            // veyr very edge case
            //while (strBuilder[0] == '+')
            //    strBuilder.Remove(0, 1);

            long computedValue = long.Parse(strBuilder.ToString());

            if (computedValue > int.MaxValue)
                computedValue = int.MaxValue;

            if (computedValue < int.MinValue)
                computedValue = int.MinValue;

            return (int)computedValue;
        }

        public static bool IsValidChar(char inputValue)
        {
            string validValues = "-+0123456789 ";
            char[] validChars = validValues.ToCharArray();

            for (int i = 0; i < validChars.Length; i++)
                if (inputValue == validChars[i])
                    return true;

            return false;
        }
    }

    public static class StringToIntegerDriverCode
    {
        public static void Run()
        {
            string input = "";
            int outputInt = StringToInteger.MyAtoi(input);

            Console.WriteLine($"Input String {input}. Output int => {outputInt}.");
            Console.WriteLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Arrays
{
    public static class SearchInRotatedSortedArray
    {
        public static int Search(int[] inptArray, int target)
        {
            return BinarySearch(inptArray, 0, inptArray.Length - 1, target);
        }

        public static int BinarySearch(int[] inputArray, int start, int end, int target)
        {
            if (start >= end)
                return -1;

            int mid = start + ((end - start) / 2);

            if (target == inputArray[mid])
                return mid;
            // if middle is greater than the start => left is sorted
            else if (inputArray[mid] >= inputArray[start])
            {
                // if target less than the mid, search left
                if (target >= inputArray[start] && target < inputArray[mid])
                    return BinarySearch(inputArray, start, mid - 1, target);
                else
                    return BinarySearch(inputArray, mid + 1, end, target);

            }
            // else, right is sorted, search right
            // if (inputArray[mid] < inputArray[end])
            else
            {
                if (target > inputArray[mid] && target <= inputArray[end])
                    return BinarySearch(inputArray, mid + 1, end, target);
                else
                    return BinarySearch(inputArray, start, mid - 1, target);

            }
        }
    }

    public static class SearchInRotatedSortedArrayDriverCode
    {
        public static void Run()
        {
            int[] inputArray1 = new int[] { 4, 5, 6, 7, 0, 1, 2 };
            double median = SearchInRotatedSortedArray.Search(inputArray1, 3);

            Console.WriteLine($"Search Index => {median}.");
            Console.WriteLine();
        }
    }
}

﻿using System;

namespace CompSci.Fundamentals.ProblemSolving.Arrays
{
    public static class FindMedianSortedArrays
    {
        public static double Find(int[] nums1, int[] nums2)
        {
            int left = 0;
            int right = 0;

            int mergeIndex = 0;
            int[] sortedMergedArray = new int[nums1.Length + nums2.Length];

            while (left < nums1.Length && right < nums2.Length)
            {
                if (nums1[left] <= nums2[right])
                {
                    sortedMergedArray[mergeIndex] = nums1[left];
                    left++;
                }
                else
                {
                    sortedMergedArray[mergeIndex] = nums2[right];
                    right++;
                }
                mergeIndex++;
            }

            if (left < nums1.Length)
            {
                for (int i = left; i < nums1.Length; i++)
                {
                    sortedMergedArray[mergeIndex] = nums1[i];
                    mergeIndex++;
                }
            }

            if (right < nums2.Length)
            {
                for (int i = right; i < nums2.Length; i++)
                {
                    sortedMergedArray[mergeIndex] = nums2[i];
                    mergeIndex++;
                }
            }

            int mid = 0 + (sortedMergedArray.Length - 1) / 2;
            if (sortedMergedArray.Length % 2 == 0)
            {
                double median = ((double)sortedMergedArray[mid] + (double)sortedMergedArray[mid + 1]) / 2;
                return median;
            }

            return sortedMergedArray[mid];
        }
    }

    public static class FindMedianSortedArraysDriverCode
    {
        public static void Run()
        {
            int[] inputArray1 = new int[] { 3, 4};
            int[] inputArray2 = new int[] { };
            double median = FindMedianSortedArrays.Find(inputArray1, inputArray2);

            Console.WriteLine($"Median => {median}.");
            Console.WriteLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Arrays
{
    public static class ContainerWithMostWater
    {
        public static int MaxArea(int[] height)
        {
            int leftIndex = 0;
            int rightIndex = height.Length - 1;

            int maxArea = 0;
            while (leftIndex < rightIndex)
            {
                int currentArea = (rightIndex - leftIndex) * Math.Min(height[leftIndex], height[rightIndex]);
                maxArea = Math.Max(maxArea, currentArea);

                if (height[leftIndex] > height[rightIndex])
                    leftIndex++;
                else
                    rightIndex--;
            }

            return maxArea;
        }
    }

    public static class ContainerWithMostWaterDriverCode
    {
        public static void Run()
        {
            int outputInt = ContainerWithMostWater.MaxArea(new int[] { 1, 4, 6, 5, 3, 7, 6, 27 });

            Console.WriteLine($"MaxArea => {outputInt}.");
            Console.WriteLine();
        }
    }
}

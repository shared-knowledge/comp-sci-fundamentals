﻿using System;

namespace CompSci.Fundamentals.ProblemSolving.Arrays
{
    public static class MaxSumOfKConsecutiveElements
    {
        public static int Find(int[] inputArray, int sumCount)
        {
            int maxSum = int.MinValue;

            if (sumCount > inputArray.Length)
                throw new Exception("Sub Array Count cannot be greater than Input Array Length");

            // Note the play on indexes here. Be very careful
            for (int i = 0; i < inputArray.Length - sumCount + 1; i++)
            {
                int runnerIndex = i + sumCount - 1;

                int currentSum = 0;
                for (int j = i; j <= runnerIndex; j++)
                    currentSum += inputArray[j];

                if (currentSum > maxSum)
                    maxSum = currentSum;
            }

            return maxSum;
        }
    }

    public static class MaxSumOfKConsecutiveElementsDriverCode
    {
        public static void Run()
        {
            int[] inputArray = new int[] { 1, 4, 2, 10, 23, 3, 1, 0, 20 };
            int consecutiveCount = 4;
            int maxSum = MaxSumOfKConsecutiveElements.Find(inputArray, consecutiveCount);

            Console.WriteLine($"Max Sum Of {consecutiveCount} ConsecutiveElements => {maxSum}.");
            Console.WriteLine();
        }
    }
}

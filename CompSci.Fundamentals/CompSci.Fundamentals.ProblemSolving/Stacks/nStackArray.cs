﻿using System;
using System.Collections.Generic;

namespace CompSci.Fundamentals.ProblemSolving.Stacks
{
    /// <summary>
    /// There is wastage of space in this implementation. 
    /// A more efficient solution would be to use mani array plus 2 extra integer array to store array index pointers for top[] and next[]
    /// 
    /// Also, don't forget to implement StackOverflow Exception.                 
    /// </summary>
    public class nStackArray
    {
        private string[] _baseArray;
        private readonly int _stackSize;
        private readonly int _numberOfStacks;
        private const int _baseArraySize = 16;

        private readonly List<StackIndex> _stackIndexes;

        public nStackArray(int numberOfStacks = 4)
        {
            if (numberOfStacks < 1 || numberOfStacks > 8 || numberOfStacks % 2 != 0)
                throw new Exception("Number of Stacks should be an even number between 1 & 8.");

            _numberOfStacks = numberOfStacks;
            _baseArray = new string[_baseArraySize];
            _stackSize = _baseArraySize / numberOfStacks;

            _stackIndexes = new List<StackIndex>();
            for (int i = 0; i < _numberOfStacks; i++)
            {
                _stackIndexes.Add(new StackIndex
                {
                    StackCount = i,
                    StartIndex = (_stackSize * i),
                    CurrentIndex = -1
                });
            }
        }

        public void Push(string data, int stackNumber)
        {
            if (string.IsNullOrEmpty(data))
                throw new Exception("Null or empty data cannot be added to stack.");

            if (stackNumber < 0 || stackNumber > _numberOfStacks)
                throw new Exception($"Stack not found. Available number of stacks = {_numberOfStacks}. Count is zero based.");

            StackIndex stackIndex = GetCurrentStackIndex(stackNumber);
            int baseindex = ++stackIndex.CurrentIndex + (_stackSize * stackIndex.StackCount);

            if (baseindex >= stackIndex.StartIndex + _stackSize)
                throw new Exception($"Array number {stackNumber} is full. Stackoverflow Exception");

            _baseArray[baseindex] = data;
        }

        public string Pop(int stackNumber)
        {
            StackIndex stackIndex = ValidateStackForPopOrPeek(stackNumber);
            return _baseArray[stackIndex.CurrentIndex--];
        }

        public string Peek(int stackNumber)
        {
            StackIndex stackIndex = ValidateStackForPopOrPeek(stackNumber);
            return _baseArray[stackIndex.CurrentIndex];
        }

        private StackIndex ValidateStackForPopOrPeek(int stackNumber)
        {
            StackIndex stackIndex = GetCurrentStackIndex(stackNumber);

            if (stackIndex == null)
                throw new Exception($"Stack number {stackNumber} not found. Max stack number is {_numberOfStacks} with a size of {_stackSize}.");

            if (stackIndex.CurrentIndex < 0)
                throw new Exception($"Cannot Pop Empty Stack. Stack {stackNumber} is empty.");

            return stackIndex;
        }

        private StackIndex GetCurrentStackIndex(int stackNumber) => _stackIndexes[stackNumber - 1];

        private class StackIndex
        {
            public int StackCount { get; set; }
            public int StartIndex { get; set; }
            public int CurrentIndex { get; set; }
        }

        public void PrintStack(int stackNumber)
        {
            StackIndex stackIndex = GetCurrentStackIndex(stackNumber);
            for (int i = stackIndex.StartIndex; i <= stackIndex.StartIndex + stackIndex.CurrentIndex; i++)
                Console.WriteLine($"Printing data for stack number {stackIndex.StackCount} index = [{_baseArray[i]}] ");
        }
    }

    public static class nStackArrayDriverCode
    {
        public static void Run()
        {
            nStackArray myStackArray = new nStackArray(4);

            Console.WriteLine($"============= Stack 1================");
            myStackArray.Push("first stack, first entry", 1);
            myStackArray.Push("first stack, second entry", 1);
            myStackArray.Push("first stack, third entry", 1);
            myStackArray.Push("first stack, fourth entry", 1);
            myStackArray.Push("first stack, fifth entry", 1);

            myStackArray.PrintStack(1);
            //myStackArray.Pop(2); // to test popping an empty stack in the array

            Console.WriteLine($"============= Stack 2================");
            myStackArray.Push("second stack, first entry", 2);
            myStackArray.PrintStack(2);
            Console.WriteLine($"========== Stack 2 Popped============");
            myStackArray.Pop(2);
            myStackArray.PrintStack(2);

            Console.WriteLine($"============= Stack 3================");
            myStackArray.Push("third stack, first entry", 3);
            myStackArray.Push("third stack, second entry", 3);
            myStackArray.Push("third stack, third entry", 3);
            myStackArray.Push("third stack, fourth entry", 3);
            myStackArray.Push("third stack, fifth entry", 3);

            myStackArray.PrintStack(3);

            Console.WriteLine($"========== Stack 3 Popped============");
            Console.WriteLine($"Popping off stack 3");
            myStackArray.Pop(3);

            Console.WriteLine($"");
            myStackArray.PrintStack(3);

            Console.WriteLine($"============= Stack 4================");
            myStackArray.Push("fourth stack, first entry", 4);
            myStackArray.PrintStack(4);
            Console.WriteLine($"============= Done ================");
        }
    }
}

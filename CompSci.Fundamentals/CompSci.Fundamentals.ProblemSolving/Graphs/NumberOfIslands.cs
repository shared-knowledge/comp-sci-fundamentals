﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Graphs
{
    public class NumberOfIslands
    {
        public int NumIslands(char[][] grid)
        {
            if (grid == null || grid.Length == 0)
                return 0;

            int islandCount = 0;
            for (int i = 0; i < grid.Length; i++)
            {
                for (int j = 0; j < grid[i].Length; j++)
                {
                    if (grid[i][j] == '1')
                    {
                        islandCount++;
                        SearchNeighorGrids(grid, i, j);
                    }
                }
            }

            return islandCount;
        }

        // if this function returns, it means it couldn't find any land to recursively go through, 
        // thus, we have an island
        public void SearchNeighorGrids(char[][] grid, int rowIndex, int columnIndex)
        {
            int rowLength = grid.Length;
            int columnLength = grid[0].Length;

            // validate row index
            if (rowIndex < 0 || rowIndex >= rowLength)
                return;

            // validate column index
            if (columnIndex < 0 || columnIndex >= columnLength)
                return;

            // check if grid spot has been visited before
            // if it has, just return
            if (grid[rowIndex][columnIndex] == '0')
                return;

            // set grid spot as visited
            grid[rowIndex][columnIndex] = '0';

            SearchNeighorGrids(grid, rowIndex - 1, columnIndex);
            SearchNeighorGrids(grid, rowIndex + 1, columnIndex);
            SearchNeighorGrids(grid, rowIndex, columnIndex - 1);
            SearchNeighorGrids(grid, rowIndex, columnIndex + 1);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Graphs
{
    public class CutOffTreesForGolfEvent
    {
        // Tree Node Count
        private int TreeNodeCount = 0;

        // dictionary of location nodes
        private Dictionary<Tuple<int, int>, MyGraphNode> AllNodes = new Dictionary<Tuple<int, int>, MyGraphNode>();

        public int CutOffTree(IList<IList<int>> forest)
        {
            // build ordered tree nodes list
            BuildGolfCourseGraph(forest);

            // init node queue
            Queue<MyGraphNode> nodeQueue = new Queue<MyGraphNode>();

            // get node for start location and enqueue     

            MyGraphNode startNode = GetOrCreateNode(forest[0][0], new int[2] { 0, 0 });
            nodeQueue.Enqueue(startNode);

            int steps = -1;
            while (nodeQueue.Count > 0)
            {
                MyGraphNode currentLocation = nodeQueue.Dequeue();

                if (!currentLocation.Visited)
                {
                    for (int i = 0; i < currentLocation.Neighbors.Count; i++)
                    {
                        MyGraphNode neighborNode = currentLocation.Neighbors[i];

                        if (neighborNode.Height >= 1)
                        {
                            if (neighborNode.Height > currentLocation.Height || neighborNode.Height == 1)
                                nodeQueue.Enqueue(neighborNode);
                        }
                    }

                    currentLocation.SetVisited();
                    steps++;
                }
            }

            if (steps == TreeNodeCount)
                return steps;

            return -1;
        }

        public void BuildGolfCourseGraph(IList<IList<int>> forest)
        {
            for (int i = 0; i < forest.Count; i++)
            {
                for (int j = 0; j < forest[i].Count; j++)
                {
                    MyGraphNode treeNode = GetOrCreateNode(forest[i][j], new int[] { i, j });

                    // check for neighbors and add
                    AddTreeNeighbors(forest, new int[] { i - 1, j }, treeNode);
                    AddTreeNeighbors(forest, new int[] { i + 1, j }, treeNode);
                    AddTreeNeighbors(forest, new int[] { i, j - 1 }, treeNode);
                    AddTreeNeighbors(forest, new int[] { i, j + 1 }, treeNode);
                }
            }
        }

        public void AddTreeNeighbors(IList<IList<int>> forest, int[] coOrds, MyGraphNode node)
        {
            if (IsValidCoOrdinate(coOrds, forest.Count, forest[0].Count))
                node.Neighbors.Add(GetOrCreateNode(forest[coOrds[0]][coOrds[1]], new int[] { coOrds[0], coOrds[1] }));
        }

        public MyGraphNode GetOrCreateNode(int height, int[] coOrds)
        {
            Tuple<int, int> location = new Tuple<int, int>(coOrds[0], coOrds[1]);

            if (!AllNodes.ContainsKey(location))
            {
                if (height > 1)
                    if (!location.Equals(new Tuple<int, int>(0, 0)))
                        TreeNodeCount++;

                AllNodes.Add(location, new MyGraphNode(height, coOrds));
            }

            return AllNodes.GetValueOrDefault(location);
        }

        public bool IsValidCoOrdinate(int[] coOrds, int rowLength, int columnLength)
        {
            bool validRow = false;
            bool validColumn = false;

            if (coOrds[0] >= 0 && coOrds[0] < rowLength)
                validRow = true;

            if (coOrds[1] >= 0 && coOrds[1] < rowLength)
                validColumn = true;

            return (validRow && validColumn);
        }

        public class MyGraphNode
        {
            public bool Visited { get; private set; }
            public int Height { get; private set; }
            public int[] CoOrdinates { get; private set; }

            public List<MyGraphNode> Neighbors { get; private set; }

            public MyGraphNode(int height, int[] coOrdinates)
            {
                Visited = false;
                Height = height;
                Neighbors = new List<MyGraphNode>();
                CoOrdinates = new int[2] { coOrdinates[0], coOrdinates[1] };
            }

            public void SetVisited()
            {
                Visited = true;
                Height = 1;
            }
        }
    }

    public static class CutOffTreesForGolfEventDriverCode
    {
        public static void Run()
        {
            CutOffTreesForGolfEvent cutOffTreesForGolfEvent = new CutOffTreesForGolfEvent();
            IList<IList<int>> inputList = new List<IList<int>>()
            {
                new List<int>(){ 2, 3, 4 },
                new List<int>(){ 0, 0, 5 },
                new List<int>(){ 8, 7, 6 }
            };

            int result = cutOffTreesForGolfEvent.CutOffTree(inputList);

            Console.WriteLine($"Steps to cut tree => {result}.");
            Console.WriteLine();
        }
    }
}

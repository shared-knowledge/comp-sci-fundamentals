﻿using CompSci.Fundamentals.DataStructures.Hierarchical;
using System.Collections.Generic;
using System;

namespace CompSci.Fundamentals.ProblemSolving.Graphs
{
    public static class FindBuildOrderDFS
    {
        public static MyGraphNode[] Run(string[] data, string[][] dependencies)
        {
            MyGraph myGraph = new MyGraph(data, dependencies);
            Stack<MyGraphNode> nodeStack = new Stack<MyGraphNode>();

            if (myGraph.NodeKeys.Count > 0)
            {
                for (int i = 0; i < myGraph.NodeKeys.Count; i++)
                    RunDFSOnNode(myGraph.AdjacencyList.GetValueOrDefault(myGraph.NodeKeys[i]), nodeStack);
            }

            MyGraphNode[] orderedNodes = new MyGraphNode[nodeStack.Count];

            // NOTE: NEVER EVER use a stack count for a LOOP while you POP() off the Stack. 
            // This should be obvious. Think it through
            int count = nodeStack.Count;

            for (int i = 0; i < count; i++)
                orderedNodes[i] = nodeStack.Pop();

            return orderedNodes;
        }

        private static void RunDFSOnNode(MyGraphNode node, Stack<MyGraphNode> nodeStack)
        {
            if (node.NodeState == MyGraphNodeState.IsProcessing)
                throw new Exception($"Cycle found in graph. Can NOT produce order.");

            if (node.NodeState == MyGraphNodeState.Processed)
                return;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                if (node.ChildNodes[i].NodeState == MyGraphNodeState.New)
                {
                    node.ChangeNodeState(MyGraphNodeState.IsProcessing);
                    RunDFSOnNode(node.ChildNodes[i], nodeStack);
                }
            }

            node.ChangeNodeState(MyGraphNodeState.Processed);
            nodeStack.Push(node);
        }
    }

    public static class FindBuildOrderDFSDriverCode
    {
        public static void Run()
        {
            string[] nodes = new string[] { "a", "b", "c", "d", "e", "f" };

            string[][] dependencies = new string[][]
            {
                new string[] { "a", "d" },
                new string[] { "f", "b" },
                new string[] { "b", "d" },
                new string[] { "f", "a" },
                new string[] { "d", "c" }
            };

            MyGraphNode[] orderedNodes = FindBuildOrderDFS.Run(nodes, dependencies);

            for (int i = 0; i < orderedNodes.Length; i++)
                if (orderedNodes[i] != null)
                    Console.WriteLine($"Node Data => {orderedNodes[i].Data} ");
        }
    }
}

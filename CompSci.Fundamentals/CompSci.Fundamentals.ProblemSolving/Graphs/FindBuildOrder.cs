﻿using CompSci.Fundamentals.DataStructures.Hierarchical;
using System;

namespace CompSci.Fundamentals.ProblemSolving.Graphs
{
    /// <summary>
    /// 
    /// </summary>
    public static class FindBuildOrder
    {
        public static MyCustomGraphNode[] Run(string[] parentData, string[][] dependencies)
        {
            MyCustomGraph graph = BuildGraph(parentData, dependencies);
            return OrderNodesBasedOnDependencies(graph.Nodes.ToArray());
        }


        public static MyCustomGraph BuildGraph(string[] parentData, string[][] dependencies)
        {
            MyCustomGraph graph = new MyCustomGraph();

            // we first add all nodes, to make sure even if a node does not have an edge, and thus is not added below, it would still exist in the graph
            for (int i = 0; i < parentData.Length; i++)
                graph.InsertNode(parentData[i]);

            for (int i = 0; i < dependencies.Length; i++)
            {
                string[] dependency = dependencies[i];
                graph.AddEdge(dependency[0], dependency[1]);
            }

            return graph;
        }

        public static MyCustomGraphNode[] OrderNodesBasedOnDependencies(MyCustomGraphNode[] nodes)
        {
            MyCustomGraphNode[] orderedNodes = new MyCustomGraphNode[nodes.Length];

            // Add nodes with no dependency to orderedNodes array
            int orderedCount = 0;
            orderedCount = AddNonDependantNodes(orderedNodes, nodes, orderedCount);


            // If ordered node
            int nodesToBeProcessed = 0;
            while (nodesToBeProcessed < orderedNodes.Length)
            {
                // When nodesToBeProcessed = 0, we are getting the first node from orderedNodes array and using to start building dependency.
                MyCustomGraphNode currentNode = orderedNodes[nodesToBeProcessed];

                // If we find a null node, meaning the orderedNodes array is empty, it means there is a cycle, we just return null as no build order is possible
                if (currentNode == null)
                    return null;

                MyCustomGraphNode[] childrenOfCurrentNodes = currentNode.Children.ToArray();

                for (int i = 0; i< childrenOfCurrentNodes.Length; i++)
                    childrenOfCurrentNodes[i].DecrementDependency();

                orderedCount = AddNonDependantNodes(orderedNodes, nodes, orderedCount);
                nodesToBeProcessed++;
            }

            return orderedNodes;
        }

        public static int AddNonDependantNodes(MyCustomGraphNode[] orderedNodes, MyCustomGraphNode[] unOrderedNodes, int orderedCount)
        {
            // loop through all nodes, if any nodes have dependency == 0, add to ordered list and increment ordered list count
            for (int i = 0; i < unOrderedNodes.Length; i++)
            {
                if (unOrderedNodes[i].Dependencies == 0)
                {
                    orderedNodes[orderedCount] = unOrderedNodes[i];
                    orderedCount++;
                }
            }

            return orderedCount;
        }
    }

    public static class FindBuildOrderDriverCode
    {
        public static void Run()
        {
            string[] nodes = new string[] { "a", "b", "c", "d", "e", "f" };

            string[][] dependencies = new string[][]
            {
                new string[] { "a", "d" },
                new string[] { "f", "b" },
                new string[] { "b", "d" },
                new string[] { "f", "a" },
                new string[] { "d", "c" }
            };

            MyCustomGraphNode[] orderedNodes = FindBuildOrder.Run(nodes, dependencies);

            for (int i = 0; i < orderedNodes.Length; i++)
                Console.WriteLine($"Node Data => {orderedNodes[i].Data} ");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.ProblemSolving.Graphs
{
    public class WordLadderLength
    {
        Dictionary<string, List<string>> AdjacencyList = new Dictionary<string, List<string>>();

        public int LadderLength(string beginWord, string endWord, IList<string> wordList)
        {
            if (string.IsNullOrEmpty(beginWord))
                return 0;

            if (!wordList.Contains(endWord))
                return 0;

            // build tranform list for begin word and all words in wordList
            AddTransformsToAdjacencyList(beginWord);

            foreach (string word in wordList)
                AddTransformsToAdjacencyList(word);

            // create word queue and queue begin word to get started
            Queue<KeyValuePair<string, int>> _wordQueue = new Queue<KeyValuePair<string, int>>();
            _wordQueue.Enqueue(new KeyValuePair<string, int>(beginWord, 1));

            // initialize list to keep track of visited words
            List<string> visited = new List<string>();

            while (_wordQueue.Count > 0)
            {
                int queueSize = _wordQueue.Count;

                // process for only size of queue when we enter level
                for (int i = 0; i < queueSize; i++)
                {
                    KeyValuePair<string, int> dequeuedNode = _wordQueue.Dequeue();
                    string dequeuedWord = dequeuedNode.Key;
                    int level = dequeuedNode.Value;

                    if (dequeuedWord == endWord)
                        return level;

                    if (!visited.Contains(dequeuedWord))
                    {
                        visited.Add(dequeuedWord);

                        List<string> transforms = GetWordTransforms(dequeuedWord);

                        foreach (string transform in transforms)
                        {
                            if (AdjacencyList.ContainsKey(transform))
                            {
                                List<string> transformWordsList = AdjacencyList.GetValueOrDefault(transform);

                                foreach (string word in transformWordsList)
                                {
                                    if (!visited.Contains(word))
                                    {
                                        // check if word is equal to target
                                        if (word.Equals(endWord))
                                            return level + 1;

                                        // enqueue word if it is not equal to target
                                        // we'd analyze separately
                                        _wordQueue.Enqueue(new KeyValuePair<string, int>(word, level + 1));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return -1;
        }

        // transform as key
        public void AddTransformsToAdjacencyList(string inputValue)
        {
            for (int i = 0; i < inputValue.Length; i++)
            {
                // formulate new transform
                StringBuilder str = new StringBuilder(inputValue);
                str[i] = '*';
                string newTransform = str.ToString();

                // get wordsList for transform
                List<string> transformList = AdjacencyList.GetValueOrDefault(newTransform);

                // if wordsList for transform is empty, create one and add word
                if (transformList == null)
                    AdjacencyList.Add(newTransform, new List<string>() { inputValue });
                else
                    transformList.Add(inputValue);
            }
        }

        public List<string> GetWordTransforms(string inputValue)
        {
            List<string> transforms = new List<string>();

            for (int i = 0; i < inputValue.Length; i++)
            {
                // formulate new transform
                char[] newString = inputValue.ToCharArray();
                newString[i] = "*".ToCharArray()[0];
                string newTransform = new string(newString);

                transforms.Add(newTransform);
            }

            return transforms;
        }
    }

    public static class WordLadderLengthDriverCode
    {
        public static void Run()
        {
            WordLadderLength wordLadderLength2 = new WordLadderLength();
            int length = wordLadderLength2.LadderLength("hit", "cog", new List<string> { "hot", "dot", "dog", "lot", "log", "cog" });

            Console.WriteLine($"Ladder Length => {length}");
        }
    }
}

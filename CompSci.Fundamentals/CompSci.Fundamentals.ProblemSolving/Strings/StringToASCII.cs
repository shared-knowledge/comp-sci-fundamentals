﻿using System;

namespace CompSci.Fundamentals.ProblemSolving.Strings
{
    public class StringToASCII
    {
        private static string _alphabets = "abcdefghijklmnopqrstuvwxyz";

        public static void Convert()
        {
            char[] alphabetChars = _alphabets.ToCharArray();

            foreach (char c in alphabetChars)
                Console.WriteLine($"{(int)c} ");

            Console.WriteLine();
        }
    }
}

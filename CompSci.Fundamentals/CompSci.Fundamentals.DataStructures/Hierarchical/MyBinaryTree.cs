﻿using System;
using System.Collections.Generic;

namespace CompSci.Fundamentals.DataStructures.Hierarchical
{
    /// <summary>
    /// Although the root class here is just called a Binary Tree, we have actually implemented a Binary Search Tree. Remember the difference!
    /// </summary>
    public class MyBinaryTree
    {
        public MyTreeNode CreateBSTFromArray(int[] inputArray, int startPointer, int endPointer)
        {
            // the input array is typically given sorted
            // however, to make it obvious that for creating a BST it has to be sorted, i have included the step here
            Array.Sort(inputArray);

            // check base case
            if (startPointer > endPointer)
                return null;

            // find pointer element
            int mid = startPointer + (endPointer - startPointer) / 2;

            MyTreeNode rootNode = new MyTreeNode(inputArray[mid]);
            rootNode.LeftNode = CreateBSTFromArray(inputArray, startPointer, mid - 1);
            rootNode.RightNode = CreateBSTFromArray(inputArray, mid + 1, endPointer);

            return rootNode;
        }

        public MyTreeNode InsertIntoBSTIterative(MyTreeNode node, int data)
        {
            if (node == null)
                return new MyTreeNode(data);

            MyTreeNode rootNode = node;

            while (node != null)
            {
                if (data <= node.Data)
                {
                    if (node.LeftNode == null)
                    {
                        node.LeftNode = new MyTreeNode(data);
                        break;
                    }
                    else
                    {
                        node = node.LeftNode;
                    }
                }

                if (data > node.Data)
                {
                    if (node.RightNode == null)
                    {
                        node.RightNode = new MyTreeNode(data);
                        break;
                    }
                    else
                    {
                        node = node.RightNode;
                    }
                }
            }

            return rootNode;
        }

        public MyTreeNode InsertIntoBSTRecursive(MyTreeNode node, int data)
        {
            if (node == null)
                return new MyTreeNode(data);

            MyTreeNode rootNode = node;

            if (data < node.Data)
                if (node.LeftNode != null)
                    InsertIntoBSTRecursive(node.LeftNode, data);
                else
                    node.LeftNode = new MyTreeNode(data);

            if (data > node.Data)
                if (node.RightNode != null)
                    InsertIntoBSTRecursive(node.RightNode, data);
                else
                    node.RightNode = new MyTreeNode(data);

            return rootNode;
        }

        // I tried counting all the nodes in a Tree here without BFS or DFS, instead of just counting the leaf nodes.
        // A bit difficult as there is no clear Base & Recursive case. 
        // basically, counting all nodes == traversing the tree.
        // You simply can't efficiently visit all nodes in a Tree efficiently without a Travesal algorithm.
        public int CountLeafNodesInTreeRecursive(MyTreeNode node)
        {
            if (node == null)
                return 0;

            if (node.LeftNode == null && node.RightNode == null)
                return 1;

            return CountLeafNodesInTreeRecursive(node.LeftNode) + CountLeafNodesInTreeRecursive(node.RightNode);
        }

        public void PrintTreeUsingBFS(MyTreeNode node)
        {
            Console.WriteLine($" ============= Printing Queue Breadth First =============== ");

            Queue<MyTreeNode> nodeQueue = new Queue<MyTreeNode>();

            nodeQueue.Enqueue(node);

            while (nodeQueue.Count > 0)
            {
                MyTreeNode tempNode = nodeQueue.Dequeue();
                Console.WriteLine($"Data --> {tempNode.Data} ");

                if (tempNode.LeftNode != null)
                    nodeQueue.Enqueue(tempNode.LeftNode);

                if (tempNode.RightNode != null)
                    nodeQueue.Enqueue(tempNode.RightNode);
            }
        }


        private Stack<MyTreeNode> _inOrderNodeStack = new Stack<MyTreeNode>();
        public void PrintTreeUsingDFSInOrder(MyTreeNode node)
        {
            if (node == null)
                return;

            if (node.LeftNode != null)
            {
                _inOrderNodeStack.Push(node.LeftNode);
                PrintTreeUsingDFSInOrder(node.LeftNode);
            }

            Console.WriteLine($"Data --> {node.Data} ");

            if (node.RightNode != null)
            {
                _inOrderNodeStack.Push(node.RightNode);
                PrintTreeUsingDFSInOrder(node.RightNode);
            }
        }

        private Stack<MyTreeNode> _inOrderNodeStackReverse = new Stack<MyTreeNode>();
        public void PrintTreeUsingDFSInOrderReverse(MyTreeNode node)
        {
            if (node == null)
                return;

            if (node.RightNode != null)
            {
                _inOrderNodeStackReverse.Push(node.RightNode);
                PrintTreeUsingDFSInOrderReverse(node.RightNode);
            }

            Console.WriteLine($"Data --> {node.Data} ");

            if (node.LeftNode != null)
            {
                _inOrderNodeStackReverse.Push(node.LeftNode);
                PrintTreeUsingDFSInOrderReverse(node.LeftNode);
            }
        }


        private Stack<MyTreeNode> _preOrderNodeStack = new Stack<MyTreeNode>();
        public void PrintTreeUsingDFSPreOrder(MyTreeNode node)
        {
            if (node == null)
                return;

            Console.WriteLine($"Data --> {node.Data} ");

            if (node.LeftNode != null)
            {
                _preOrderNodeStack.Push(node.LeftNode);
                PrintTreeUsingDFSInOrder(node.LeftNode);
            }

            if (node.RightNode != null)
            {
                _preOrderNodeStack.Push(node.RightNode);
                PrintTreeUsingDFSInOrder(node.RightNode);
            }
        }
    }

    public class MyTreeNode
    {
        public int Data { get; private set; }
        public MyTreeNode LeftNode { get; set; }
        public MyTreeNode RightNode { get; set; }

        public MyTreeNode(int data)
        {
            Data = data;
            LeftNode = null;
            RightNode = null;
        }
    }

    public static class MyBinaryTreeDriverCode
    {
        public static void Run()
        {
            int[] inputArray = new int[] { 1, 2, 3, 4, 5, 6, 7 };

            MyBinaryTree myBinaryTree = new MyBinaryTree();
            MyTreeNode node = myBinaryTree.CreateBSTFromArray(inputArray, 0, inputArray.Length - 1);

            MyTreeNode updatedNode = myBinaryTree.InsertIntoBSTIterative(node, 8);
            MyTreeNode updatedNode1 = myBinaryTree.InsertIntoBSTIterative(node, 7);

            MyTreeNode updatedNode2 = myBinaryTree.InsertIntoBSTRecursive(node, 9);
            MyTreeNode updatedNode3 = myBinaryTree.InsertIntoBSTRecursive(node, 0);

            int numberOfNodes = myBinaryTree.CountLeafNodesInTreeRecursive(node);
            //int numberOfNodess = myBinaryTree.CountLeafNodesInTreeIterative(node);

            myBinaryTree.PrintTreeUsingBFS(node);

            Console.WriteLine($" ============= Printing Queue Depth First InOrder =============== ");

            myBinaryTree.PrintTreeUsingDFSInOrder(node);

            Console.WriteLine($" ============= Printing Queue Depth First InOrder Reverse =============== ");

            myBinaryTree.PrintTreeUsingDFSInOrderReverse(node);

            Console.WriteLine($" ============= Printing Queue Depth First PreOrder =============== ");

            myBinaryTree.PrintTreeUsingDFSPreOrder(node);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.DataStructures.Hierarchical
{

    /// <summary>
    /// A few tricks here;
    /// 1.) Be VERY CAREFUL with your INSERT.
    ///     --> To maintain order, we index incoming words based on their ASCII codes - the ASCII code for 'a'. That way, we keep things ordered.
    ///     --> Also, make sure to pass everything to lowercase, to ensure ascii order. 'a' != 'A'. See "GlobalStartIndex = (int)'a'"
    ///     
    /// 2.) For Searching, we use a private method "AddAllWordsForPrefix".
    ///     This recursively goes through all child nodes and prints out possible words, using isEndOfWord marker on nodes
    ///     
    /// 3.) For this "AddAllWordsForPrefix" methods, the trick here was to pass in the list of words in the method signature.
    ///     That way, we maintain our list through the recursion
    ///     
    /// 4.) On DELETE. Be Careful.
    ///     Manipulate your "node.IsEndOfWord" at the end of you Add and Delet Methods
    /// </summary>
    public class MyTrieTree
    {
        public int GlobalStartIndex = (int)'a';
        public MyTrieNode RootNode { get; set; }
        public MyTrieTree() => RootNode = new MyTrieNode();

        public void Add(string[] input)
        {
            for (int i = 0; i < input.Length; i++)
                Add(input[i]);
        }

        public void Add(string input)
        {
            // so that we know what ascii code range we using
            input = input.ToLower();
            char[] inputArray = input.ToCharArray();

            MyTrieNode node = RootNode;

            int charCount = 0;
            while (charCount < inputArray.Length)
            {
                // formulate incoming node
                MyTrieNode newNode = new MyTrieNode();
                newNode.Data = inputArray[charCount];

                // check if newNode exists in "node" and return existing trie node to get links
                MyTrieNode newNodeExistsInRoot = ExistsInNode(node, newNode);

                // insert and return if not exist, else just return existing node
                if (newNodeExistsInRoot != null)
                    node = newNodeExistsInRoot;
                else
                    node = InsertInNode(node, newNode);

                charCount++;
            }
            
            // mark node as end of word
            node.IsEndOfWord = true;
        }

        public void Delete(string input)
        {
            input = input.ToLower();
            char[] inputArray = input.ToCharArray();

            MyTrieNode node = RootNode;

            int count = 0;
            while (count < inputArray.Length)
            {
                int index = inputArray[count] - GlobalStartIndex;
                MyTrieNode temp = node.Children[index];

                if (temp == null)
                    return;

                node = temp;
                count++;
            }
            
            node.IsEndOfWord = false;
        }

        public string[] GetAllWords()
        {
            List<string> words = new List<string>();

            AddAllWordsToPrefix(RootNode, null, words);

            return words.ToArray();
        }

        public string[] SearchPrefix(string input)
        {
            // so that we know what ascii code range we using
            input.ToLower();

            char[] inputArray = input.ToCharArray();
            
            MyTrieNode node = RootNode;

            StringBuilder stringBuilder = new StringBuilder();

            int charCount = 0;
            while (charCount < inputArray.Length)
            {
                MyTrieNode targetNode = new MyTrieNode();
                targetNode.Data = input[charCount];

                MyTrieNode existingNode = ExistsInNode(node, targetNode);
                stringBuilder.Append(existingNode.Data);
                node = existingNode;

                charCount++;
            }

            List<string> words = new List<string>();

            AddAllWordsToPrefix(node, stringBuilder.ToString(), words);

            return words.ToArray();
        }

        private MyTrieNode InsertInNode(MyTrieNode rootNode, MyTrieNode newNode)
        {
            if (newNode == null)
                return rootNode;

            int insertIndex = newNode.Data - GlobalStartIndex;
            rootNode.Children[insertIndex] = newNode;

            return rootNode.Children[insertIndex];
        }

        private MyTrieNode ExistsInNode(MyTrieNode rootNode, MyTrieNode newNode)
        {
            if (newNode == null) 
                return null;

            int nodeIndex = newNode.Data - GlobalStartIndex;
            return rootNode.Children[nodeIndex];
        }

        private void AddAllWordsToPrefix(MyTrieNode prefixNode, string prefix, List<string> words)
        {
            if (prefixNode.IsEndOfWord)
                words.Add(prefix);

            for (int i = 0; i < prefixNode.Children.Length; i++)
            {
                MyTrieNode child = prefixNode.Children[i];
                StringBuilder stringBuilder = new StringBuilder(prefix);

                // Base Case : if child == null, break
                // Recursive Case : if child != null, recurse children of child
                if (child != null)
                {
                    stringBuilder.Append(child.Data);
                    string newPrefix = stringBuilder.ToString();

                    AddAllWordsToPrefix(child, newPrefix, words);
                }
            }
        }
    }

    public class MyTrieNode
    {
        public char Data { get; set; }
        public bool IsEndOfWord { get; set; }
        public MyTrieNode[] Children { get; private set; }

        public MyTrieNode()
        {
            Children = new MyTrieNode[26];
        }
    }

    public static class MyTrieTreeDriverCode
    {
        public static void Run()
        {
            Console.WriteLine($"================ My Trie tree ====================");

            MyTrieTree myTrieTree = new MyTrieTree();

            //myTrieTree.Add("tr"); // if you don't add 'tr' as a valid word, it won't return during prefix search
            myTrieTree.Add("try");
            myTrieTree.Add("tree");
            myTrieTree.Add("trie");
            myTrieTree.Add("TRYING");
            myTrieTree.Add("tryings");
            myTrieTree.Add("trip");
            myTrieTree.Add("true");
            myTrieTree.Add("algo");
            myTrieTree.Add("string");
            myTrieTree.Add("struct");

            string[] find = myTrieTree.SearchPrefix("tr");

            myTrieTree.Delete("trip");
            myTrieTree.Delete("trying");
            string[] finds = myTrieTree.SearchPrefix("tr");

            myTrieTree.Add("trip");
            myTrieTree.Add("trying");
            myTrieTree.Add("triping");
            string[] findss = myTrieTree.SearchPrefix("tr");

            //myTrieTree.Add("mouse");
            //myTrieTree.Add("mobile");
            //myTrieTree.Add("moneypot");
            //myTrieTree.Add("monitor");
            //myTrieTree.Add("mousepad");

            //string[] found = myTrieTree.SearchPrefix("mo");
            //string[] newFind = myTrieTree.SearchPrefix("mou");




            //string[] inputStringArray = new string[] { "geeks", "for", "geeks", "a", "portal", "to", "learn", "can", "be", "computer", "science", "zoom", "yup", "fire", "in", "data" };
            //myTrieTree.Add(inputStringArray);
            //string[] words = myTrieTree.GetAllWords();



            //string[] inputStringArray = new string[] { "ice", "cream", "icecream" };
            //myTrieTree.Add(inputStringArray);

            //string[] found = myTrieTree.SearchPrefix("ice");
        }
    }
}

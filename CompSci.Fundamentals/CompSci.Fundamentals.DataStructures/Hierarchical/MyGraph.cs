﻿using System.Collections.Generic;

namespace CompSci.Fundamentals.DataStructures.Hierarchical
{
    /// <summary>
    /// 1.) We using a List<> here and NOT an array '[]'.
    ///     This is because we want a dynamically resizing array; an ArrayList.
    /// 2.) As with any Linked Data structre, when we insert a new Node, we always want to return the node from the exisitng Data Structure so we get it's links.
    ///     This should be obvious, think it through.
    /// 3.) When Building the Graph and Running a DFS on it, always use the nodes from the adjacency list as they have the     
    /// </summary>
    public class MyGraph
    {
        public List<string> NodeKeys { get; private set; }
        public Dictionary<string, MyGraphNode> AdjacencyList { get; private set; }

        public MyGraph()
        {
            NodeKeys = new List<string>();
            AdjacencyList = new Dictionary<string, MyGraphNode>();
        }

        public MyGraph(string[] nodeData, string[][] nodeEdges)
        {
            NodeKeys = new List<string>();
            AdjacencyList = new Dictionary<string, MyGraphNode>();
            BuildGraph(nodeData, nodeEdges);
        }

        public void BuildGraph(string[] nodeData, string[][] nodeEdges)
        {
            for (int i = 0; i < nodeData.Length; i++)
                InsertOrGetNode(nodeData[i]);

            for (int i = 0; i < nodeEdges.Length; i++)
            {
                string[] edge = nodeEdges[i];
                AddEdge(edge[0], edge[1]);
            }
        }

        public void AddEdge(string parentData, string dependentData)
        {
            MyGraphNode parentNode = InsertOrGetNode(parentData);
            MyGraphNode dependentNode = InsertOrGetNode(dependentData);

            parentNode.ChildNodes.Add(dependentNode);
        }

        
        public MyGraphNode InsertOrGetNode(string data)
        {
            if (!AdjacencyList.ContainsKey(data))
            {
                NodeKeys.Add(data);
                AdjacencyList.Add(data, new MyGraphNode(data));
            }

            return AdjacencyList.GetValueOrDefault(data);
        }
    }

    public class MyGraphNode
    {
        public string Data { get; private set; }
        public MyGraphNodeState NodeState { get; private set; }
        public List<MyGraphNode> ChildNodes { get; private set; }

        public MyGraphNode(string data)
        {
            Data = data;
            NodeState = MyGraphNodeState.New;
            ChildNodes = new List<MyGraphNode>();
        }

        public void ChangeNodeState(MyGraphNodeState state) => NodeState = state;
    }

    public enum MyGraphNodeState
    {
        New,
        IsProcessing,
        Processed
    }
}

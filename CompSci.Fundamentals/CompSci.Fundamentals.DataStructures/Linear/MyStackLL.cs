﻿using System;
using System.Collections.Generic;

namespace CompSci.Fundamentals.DataStructures.Linear
{
    /// <summary>
    /// Stack data structure implementation using a LinkedList
    /// </summary>
    public class MyStackLL
    {
        public MyStackNode Top { get; private set; }

        private List<int> MinValues;
        private int _minValue = int.MaxValue;

        public MyStackLL(int data)
        {
            Top = new MyStackNode(data);
            Top.Next = null;
            MinValues = new List<int>();
            
            ComputeMinValOnPush(data);
        }

        public void Push(int data)
        {
            // c# typically adds nulls value to collection, so we don't check for null on data here

            MyStackNode oldTop = Top;
            MyStackNode newNode = new MyStackNode(data);

            Top = newNode;
            Top.Next = oldTop;

            ComputeMinValOnPush(data);
        }

        public bool Pop()
        {
            if (IsEmpty()) return false;

            MyStackNode oldTop = Top;
            MyStackNode newHead = Top.Next;
            

            ComputeMinValOnPop();

            Top = newHead;
            oldTop = null;

            return true;
        }

        public int Peek() => Top.Data;

        public bool IsEmpty() => Top == null;

        public int GetMin() => MinValues[MinValues.Count - 1];

        public void PrintStack()
        {
            MyStackNode node = Top;

            while (node != null)
            {
                Console.WriteLine($"{node.Data} ");
                node = node.Next;
            }
        }

        private void ComputeMinValOnPop()
        {
            if (Top.Data == MinValues[MinValues.Count - 1])
                MinValues.RemoveAt(MinValues.Count-1);
        }

        private void ComputeMinValOnPush(int data)
        {
            if (data < _minValue)
            {
                MinValues.Add(data);
                _minValue = data;
            }
        }
    }

    public class MyStackNode
    {
        public int Data { get; private set; }
        public MyStackNode Next { get; set; }

        public MyStackNode(int data)
        {
            Data = data;
            Next = null;
        }
    }

    public static class MyStackLLDriverCode
    {
        public static void Run()
        {
            MyStackLL myStackLL = new MyStackLL(3);
            myStackLL.Push(1);
            myStackLL.Push(2);
            myStackLL.Push(0);
            myStackLL.Push(4);
            myStackLL.Push(5);

            Console.WriteLine($"============= Stack ================");
            myStackLL.PrintStack();

            Console.WriteLine($"Stack Array Peek => {myStackLL.Peek()}");
            Console.WriteLine($"Stack Min => {myStackLL.GetMin()}");

            myStackLL.Pop();

            Console.WriteLine($"============= Stack ================");
            myStackLL.PrintStack();

            Console.WriteLine($"Stack Array Peek => {myStackLL.Peek()}");
            Console.WriteLine($"Stack Min => {myStackLL.GetMin()}");

            myStackLL.Pop();

            Console.WriteLine($"============= Stack ================");
            myStackLL.PrintStack();

            Console.WriteLine($"Stack Array Peek => {myStackLL.Peek()}");
            Console.WriteLine($"Stack Min => {myStackLL.GetMin()}");

            myStackLL.Pop();

            Console.WriteLine($"============= Stack ================");
            myStackLL.PrintStack();

            Console.WriteLine($"Stack Array Peek => {myStackLL.Peek()}");
            Console.WriteLine($"Stack Min => {myStackLL.GetMin()}");
        }
    }
}

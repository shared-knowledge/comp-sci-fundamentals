﻿using System;

namespace CompSci.Fundamentals.DataStructures.Linear
{
    /// <summary>
    /// Stack data structure implementation using an Array. Not ideal for a few reasons;
    /// 1. Firstly, we are not really saving space on Pop-ing, as we are just moving the cursor index, but the array still has the elements. 
    ///     If we try solve this by copying over, we loose the desired time complexity of O(1) expected for Push() and Pop() operation.
    /// 2. It's not re-sizeable. To achieve this, we'd have to implement and ArrayList as the base Array. it gets messy. 
    /// 
    /// So, just use a LinkedList for this!
    /// </summary>
    public class MyStackArray
    {
        private int _count;
        private int[] _baseArray;

        public MyStackArray(int? data, int stackSize=8)
        {
            if (data == null) throw new Exception("cannot instantiate stack with null value");

            _baseArray = new int[stackSize];
            _baseArray[0] = (int)data;
            _count++;
        }

        public void Push(int data)
        {
            _baseArray[_count] = data;
            _count++;
        }

        public void Pop() => _count--;

        public bool IsEmpty() => _count == 0;

        public int Peek() => _baseArray[_count-1];


        public int Count() => _count;
        public int StackAraySize() => _baseArray.Length;

        public void PrintStack()
        {
            for (int i = 0; i < _count; i++)
                Console.WriteLine($"{_baseArray[i]} ");
        }
    }

    public static class MyStackArrayDriverCode
    {
        public static void Run()
        {
            MyStackArray myStackArray = new MyStackArray(0);
            myStackArray.Push(1);
            myStackArray.Push(2);
            myStackArray.Push(3);
            myStackArray.Push(4);
            myStackArray.Push(5);

            Console.WriteLine($"============= Stack ================");
            myStackArray.PrintStack();

            Console.WriteLine($"Stack Size => {myStackArray.Count()}");
            Console.WriteLine($"Stack Array Length => {myStackArray.StackAraySize()}");
            Console.WriteLine($"Stack Array Peek => {myStackArray.Peek()}");

            myStackArray.Pop();

            Console.WriteLine($"============= Stack ================");
            myStackArray.PrintStack();

            Console.WriteLine($"Stack Size => {myStackArray.Count()}");
            Console.WriteLine($"Stack Array Length => {myStackArray.StackAraySize()}");
            Console.WriteLine($"Stack Array Peek => {myStackArray.Peek()}");

            myStackArray.Pop();

            Console.WriteLine($"============= Stack ================");
            myStackArray.PrintStack();

            Console.WriteLine($"Stack Size => {myStackArray.Count()}");
            Console.WriteLine($"Stack Array Length => {myStackArray.StackAraySize()}");
            Console.WriteLine($"Stack Array Peek => {myStackArray.Peek()}");
        }
    }
}

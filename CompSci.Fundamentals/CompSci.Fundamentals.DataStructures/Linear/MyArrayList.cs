﻿
using System;

namespace CompSci.Fundamentals.DataStructures.Linear
{
    public class MyArrayList
    {
        private int _arraySize;
        private string[] _baseArray;

        public int Count { get; private set; }

        public MyArrayList(int arrayStartSize = 2)
        {
            Count = 0;
            _arraySize = arrayStartSize;
            _baseArray = new string[_arraySize];
        }

        public void Add(string value)
        {
            if (!string.IsNullOrEmpty(_baseArray[_arraySize-1]))
            {
                _arraySize *= 2;
                string[] newArray = new string[_arraySize];

                CopyArray(_baseArray, newArray);
                _baseArray = newArray;
            }

            _baseArray[Count] = value;
            Count++;
        }

        public bool Remove(int index)
        {
            if (index < 0 || index > Count) return false;

            for (int i = index; i < _baseArray.Length-1; i++)
                _baseArray[i] = _baseArray[i + 1];

            Count--;
            return true;
        }

        // basically move element from index 0 to index array.length-1, n number of times
        public void Rotate(int rotationDegree=1)
        {
            int rotationCount = 0;
            for (int i = 0; i <= rotationDegree; i++)
            {
                MoveIndex(_baseArray, 0, _baseArray.Length - 1);
                rotationCount++;

                if (rotationCount == rotationDegree) break;
            }
        }

        public int Size() => _arraySize;
        public string[] GetArray() => _baseArray;

        private void CopyArray(string[] fromArray, string[] toArray)
        {
            for (int i = 0; i < fromArray.Length; i++)
                toArray[i] = fromArray[i];
        }

        private void MoveIndex(string[] inputArray, int fromIndex, int toIndex)
        {
            string value = inputArray[fromIndex];

            for (int i = fromIndex; i < toIndex; i++)
                inputArray[i] = inputArray[i + 1];

            inputArray[toIndex] = value;
        }
    }

    public static class ArrayListDriverCode
    {
        public static void Run()
        {
            Console.WriteLine("Hello World!");

            MyArrayList myArrayList = new MyArrayList();

            myArrayList.Add("first");
            myArrayList.Add("second");

            Console.WriteLine($"Array Count => {myArrayList.Count}");
            Console.WriteLine($"Array Size => {myArrayList.Size()}");
            PrintArray.Print(myArrayList.GetArray());

            myArrayList.Add("third");
            myArrayList.Add("fourth");
            myArrayList.Add("fifth");
            myArrayList.Add("sixth");
            myArrayList.Add("seventh");
            myArrayList.Add("eight");

            Console.WriteLine($"Array Count => {myArrayList.Count}");
            Console.WriteLine($"Array Size => {myArrayList.Size()}");
            PrintArray.Print(myArrayList.GetArray());

            myArrayList.Add("fifth");

            Console.WriteLine($"Array Count => {myArrayList.Count}");
            Console.WriteLine($"Array Size => {myArrayList.Size()}");
            PrintArray.Print(myArrayList.GetArray());

            myArrayList.Remove(4);

            Console.WriteLine($"Array Count => {myArrayList.Count}");
            Console.WriteLine($"Array Size => {myArrayList.Size()}");
            PrintArray.Print(myArrayList.GetArray());

            myArrayList.Remove(2);

            Console.WriteLine($"Array Count => {myArrayList.Count}");
            Console.WriteLine($"Array Size => {myArrayList.Size()}");
            PrintArray.Print(myArrayList.GetArray());

            myArrayList.Rotate(3);

            Console.WriteLine($"=======Rotated Array==============");
            Console.WriteLine($"Array Count => {myArrayList.Count}");
            Console.WriteLine($"Array Size => {myArrayList.Size()}");
            PrintArray.Print(myArrayList.GetArray());

            Console.ReadLine();
        }
    }
}

﻿using System;

namespace CompSci.Fundamentals.DataStructures.Linear
{
    public class MyQueueLL
    {
        public MyQueueNode Head { get; set; }
        public MyQueueNode Tail { get; set; }

        public MyQueueLL(string data)
        {
            ValidateIncomingData(data);

            Head = new MyQueueNode(data);
            Tail = null;

            Head.Next = Tail;
        }

        public void Add(string data)
        {
            ValidateIncomingData(data);

            if (Tail != null)
            {
                MyQueueNode oldTail = Tail;

                Tail = new MyQueueNode(data);
                oldTail.Next = Tail;
            }

            if (Tail == null)
            {
                Tail = new MyQueueNode(data);
                Head.Next = Tail;
            }

            if (Head == null)
            {
                Head = new MyQueueNode(data);
                Tail = null;

                Head.Next = Tail;
            }
        }

        public string Remove()
        {
            if (Head == null) 
                throw new Exception("Queue Empty exception");

            string val = Head.Data;
            Head = Head.Next;

            return val;
        }

        public string Peek()
        {
            if (Head == null)
                throw new System.Exception("Queue Empty exception");

            if (Tail == null)
                return Head.Next.Data;

            return Tail.Data;
        }

        public bool IsEmpty() => Head == null;

        private void ValidateIncomingData(string data)
        {
            if (string.IsNullOrEmpty(data)) 
                throw new System.Exception("Cannot perform operation on queue with empty data.");
        }

        public void PrintQueue()
        {
            MyQueueNode node = Head;
            int count = 0;
            while (node != null)
            {
                Console.WriteLine($"Data {++count}: {node.Data} ");
                node = node.Next;
            }
        }
    }

    public class MyQueueNode
    {
        public string Data { get; set; }
        public MyQueueNode Next { get; set; }

        public MyQueueNode(string data)
        {
            Data = data;
            Next = null;
        }
    }

    public static class MyQueueLLDriverCode
    {
        public static void Run()
        {
            Console.WriteLine($"============== MyQueue LL ====================");

            MyQueueLL myQueueLL = new MyQueueLL("first");
            myQueueLL.Add("second");
            myQueueLL.Add("third");
            myQueueLL.Add("fourth");
            myQueueLL.Add("fifth");
            myQueueLL.Add("sixth");

            myQueueLL.PrintQueue();


            Console.WriteLine($"========= MyQueue LL : Remove () =============");

            var val0 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val0} ");
            var val1 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val1} ");
            var val2 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val2} ");
            var val3 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val3} ");
            var val4 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val4} ");
            var val5 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val5} ");

            //var vall = myQueueLL.Remove();
            //Console.WriteLine($"Removed Data : {vall} ");


            Console.WriteLine($"========= MyQueue LL : Remove () =============");
            myQueueLL.Add("seventh");
            myQueueLL.Add("eighth");
            myQueueLL.Add("nineth");

            myQueueLL.PrintQueue();

            var val6 = myQueueLL.Remove();
            Console.WriteLine($"Removed Data : {val6} ");

            myQueueLL.PrintQueue();
        }
    }
}

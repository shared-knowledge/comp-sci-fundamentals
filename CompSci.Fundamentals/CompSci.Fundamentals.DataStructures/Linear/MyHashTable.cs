﻿using System;

namespace CompSci.Fundamentals.DataStructures.Linear
{
    public class MyHashTable
    {
        private MyHashTableNode[] _baseArray;

        public MyHashTable()
        {
            _baseArray = new MyHashTableNode[10];
        }

        public bool Add(int value) => Add(value.ToString());

        public bool Add(string value)
        {
            VaidateInput(value);

            MyHashTableNode incomingNode = new MyHashTableNode(value);

            int hashKey = Hash(value);
            MyHashTableNode headNode = _baseArray[hashKey];

            if (headNode == null)
            {
                headNode = incomingNode;
                _baseArray[hashKey] = headNode;
                return true;
            }
            else
            {
                MyHashTableNode node = headNode;

                while (node != null)
                {
                    if (node.Next == null)
                        break;

                    node = node.Next;
                }

                node.Next = incomingNode;
                return true;
            }
        }

        public bool Delete(int value) => Delete(value.ToString());

        public bool Delete(string value)
        {
            VaidateInput(value);

            int hashKey = Hash(value);
            MyHashTableNode node = _baseArray[hashKey];

            if (node.Data == value)
            {
                MyHashTableNode newHead = node.Next;
                _baseArray[hashKey] = newHead;
                node = null;
                return true;
            }

            while (node.Next != null)
            {
                if (node.Next.Data == value)
                {
                    node.Next = node.Next.Next;
                    return true;
                }

                node = node.Next;
            }
            return false;
        }

        public string Search(int value) => Search(value.ToString());

        public string Search(string value)
        {
            VaidateInput(value);

            int hashKey = Hash(value);
            MyHashTableNode node = _baseArray[hashKey];

            if (node == null)
            {
                throw new Exception($"'{value} not found in hash table'");
                //return null;
            }

            while (node != null)
            {
                if (string.Equals(value, node.Data))
                    return node.Data;
                node = node.Next;
            }

            throw new Exception($"'{value} not found in hash table'");
            //return null;
        }

        private int Hash(string key)
        {
            char[] keyChars = key.ToCharArray();

            int keySum = 0;

            for (int i = 0; i < keyChars.Length; i++)
                keySum += (int)keyChars[i];

            return keySum % 10;
        }

        private void VaidateInput(string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                throw new Exception($"input value can NOT be null, empty or whitespace.");
        }

        private class MyHashTableNode
        {
            public MyHashTableNode Next { get; set; }
            public string Data { get; private set; }

            public MyHashTableNode(string data)
            {
                Data = data;
                Next = null;
            }
        }

        public void PrintHashTable()
        {
            for (int i = 0; i < _baseArray.Length; i++)
            {
                Console.WriteLine($"=======Printing values at index {i}=========");

                MyHashTableNode node = _baseArray[i];

                while (node != null)
                {
                    Console.WriteLine($"{node.Data} ");
                    node = node.Next;
                }
            }
            Console.WriteLine($"======= Done Printing =========");
        }
    }

    public static class HashTableDriverCode
    {
        public static void Run()
        {
            MyHashTable myHashTable = new MyHashTable();
            myHashTable.Add(50);
            myHashTable.Add(700);
            myHashTable.Add(76);
            myHashTable.Add(85);
            myHashTable.Add(92);
            myHashTable.Add(73);
            myHashTable.Add(101);

            myHashTable.PrintHashTable();


            Console.WriteLine();
            Console.WriteLine($"========================================== Deleting =======================================");
            myHashTable.Delete(101);
            myHashTable.Delete(85);

            myHashTable.PrintHashTable();
        }
    }
}

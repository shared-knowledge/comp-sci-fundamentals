﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompSci.Fundamentals.DataStructures.Linear
{
    /// <summary>
    /// Note : if we do not wrap a linkedlist in a class, we stand the risk of when constantly changing the head node, 
    /// other objects referencing the head, loose track as the head node stays changing. to get around this, we wrap the head node in a class
    /// 
    /// c# ONLY supports generics for reference types and NOT value types. So, you can pass in an int into a generic as it is a value type and sits on the stack.
    /// To do that, you'd have to box it. Thatv is wrap it in a class
    /// 
    /// We have also seperated the class for a LinkedList and a LinkedList node. If you do not need the safety that comes with wrapping the LinkedList in a class,
    /// you can just instantiate a LinkedList node and take it from there
    /// </summary>
    public class MyLinkedList<T> where T : class
    {
        public MyLinkedListNode<T> Head { get; private set; }

        public MyLinkedList(T data)
        {
            Head = new MyLinkedListNode<T>(data);
            Head.Next = null;
        }

        public void Add(T value)
        {
            MyLinkedListNode<T> node = Head;

            while (node.Next != null)
                node = node.Next;

            node.Next = new MyLinkedListNode<T>(value);
        }

        public void Delete(T value)
        {
            MyLinkedListNode<T> node = Head;
            MyLinkedListNode<T> goalNode = new MyLinkedListNode<T>(value);

            if (Head.Data == goalNode.Data)
            {
                Head = Head.Next;
                return;
            }

            while (node.Next != null)
            {
                MyLinkedListNode<T> prevNode = node;

                if (node.Next.Data == goalNode.Data)
                {
                    prevNode.Next = node.Next.Next;
                    break;
                }

                node = node.Next;
            }
        }

        public void Reverse() => ReverseRecursively(Head, null);

        private void ReverseRecursively(MyLinkedListNode<T> node, MyLinkedListNode<T> prev=null)
        {
            if (node == null) return;

            MyLinkedListNode<T> oldNext = node.Next;
            node.Next = prev;

            Head = node;
            if (oldNext != null)
                ReverseRecursively(oldNext, node);
        }

        public void PrintLinkedListData()
        {
            MyLinkedListNode<T> node = Head;

            while (node != null)
            {
                Console.WriteLine($"Data = {node.Data} ");
                node = node.Next;
            }
        }
    }

    public class MyLinkedListNode<T> where T : class
    {
        public T Data { get; private set; }
        public MyLinkedListNode<T> Next { get; set; }

        public MyLinkedListNode(T data)
        {
            Data = data;
            Next = null;
        }
    }

    public class MyLinkedListDriverCode
    {
        public static void Run()
        {
            MyLinkedList<string> myLinkedList = new MyLinkedList<string>("0");
            myLinkedList.Add("1");
            myLinkedList.Add("2");
            myLinkedList.Add("3");
            myLinkedList.Add("4");
            myLinkedList.Add("5");

            myLinkedList.PrintLinkedListData();
            
            Console.WriteLine($"============Reversed===========");
            myLinkedList.Reverse();
            myLinkedList.PrintLinkedListData();

            Console.WriteLine($"============Deleted===========");
            myLinkedList.Delete("5");
            myLinkedList.Delete("3");
            myLinkedList.Delete("6");
            myLinkedList.Delete("0");
            myLinkedList.PrintLinkedListData();
        }
    }
}

﻿using System;

namespace CompSci.Fundamentals.DataStructures
{
    public static class PrintArray
    {
        public static void Print(string[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"{inputArray[i] }");
            Console.WriteLine();
        }

        public static void Print(int[] inputArray)
        {
            for (int i = 0; i < inputArray.Length; i++)
                Console.WriteLine($"{inputArray[i] }");
            Console.WriteLine();
        }
    }
}
